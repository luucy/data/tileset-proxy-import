## Tileset Proxy Data Import

The LUUCY Tileset-Proxy is dynamically injecting custom attributes into the buildings layer based on their UUIDs.

This repo is showing examples on how to use the underlying data and APIs to automate uplaod of large datasets based on geolocated datasets.

### Examples

#### [MyEnergyGuide](https://myenergyguide.ch)

Showcasing a calculated `efficiencyClass` (GEAK) from MyEnergyGuide model.

- 🧑‍💻 Notebook: [`myenergyguide.ipynb`](myenergyguide.ipynb)
- 🎨 Style: [`myenergyguide.json`](myenergyguide.json)

![Example showing colored GEAK classes](img/luucy-geak-classes.jpg)
